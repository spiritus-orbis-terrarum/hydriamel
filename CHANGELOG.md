# 0.3.0

Adding CI

- Adding CI test and CI executables to the project.

# 0.2.1

Fixed Hydriamel JS bug

- Websites with JS weren't running, self calling functions replaced by brackets
scoping.

# 0.2.0

Hydriamel now supports: HTML, CSS and Javascript

- Supports: HTML, CSS and Javascript.
- Test added.
- Javascript components compile to self-calling functions by default.

# 0.1.0

Initial Version of Hydriamel

- Includes a new way to scale web development, generating HTML files starting from a "components" and "templates" folder.
- Supports only HTML.
