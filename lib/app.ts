import { isFolder } from "./utils/files.ts";
import * as config from "./utils/config.ts";
import { buildFile, builDir } from "./utils/distributable.ts";
import { noConfigFolders } from "./utils/errors.ts";

// Get the configuration file.
const hydriamelJsonFile = await config.getHydriamelFile();

// Get the configurations of the current wokspace.
const hydriamelObj = config.getHydriamelObj(hydriamelJsonFile);

// Set the configuration global.
const { componentsRoute, templatesRoute, distRoute } = hydriamelObj;

// Check if the folder provided by the config exist.
const componentsIsFolder = await isFolder(componentsRoute);
const templateIsFolder = await isFolder(templatesRoute);
if (!componentsIsFolder || !templateIsFolder) {
  console.error(noConfigFolders(componentsRoute, templatesRoute));
  Deno.exit();
}

// Get all the templates and components in the current workspace.
const { componentsFiles, templatesFiles } = await config.getWorkFlow(
  hydriamelObj,
);

// Delete the distRoute folder and create it again.
config.createEnviorment(distRoute);

// Take each template, it will be replaced by each component.
for (const templateFile of templatesFiles) {
  //  Check for all components and inject it in the template.
  for (const componentsFile of componentsFiles) {
    if (templateFile.dir) {
      // Create the folder and insert it, no more need to check.
      await builDir(hydriamelObj, templateFile);
      break;
    } else if (componentsFile.file) {
      await buildFile(hydriamelObj, templateFile, componentsFile);
    }
  }
}
