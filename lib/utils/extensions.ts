/**
 * Determines the extension of the filename.
 *
 * @param {boolean} filename - The name of the file.
 * @return {string} The file extension.
 */
const _getExtension = (filename: string): string => {
  return filename.split(".").slice(-1)[0];
};

/**
 * Determines if a file has a HTML extension.
 *
 * @param {boolean} filename - The name of the file.
 * @return {boolean}
 */
export const isHtml = (filename: string): boolean => {
  const ext = _getExtension(filename);
  return ext === "htm" || ext === "html";
};

/**
 * Determines if a file has a CSS extension.
 *
 * @param {boolean} filename - The name of the file.
 * @return {boolean}
 */
export const isCSS = (filename: string): boolean => {
  return _getExtension(filename) === "css";
};

/**
 * Determines if a file has a JS extension.
 *
 * @param {boolean} filename - The name of the file.
 * @return {boolean}
 */
export const isJS = (filename: string): boolean => {
  return _getExtension(filename) === "js";
};

/**
 * Determines if a file has a valid extension to work for.
 *
 * @param {boolean} filename - The name of the file.
 * @return {ValidHydriamelExtension}
 */
export const validHydriamelExtension = (
  filename: string,
): ValidHydriamelExtension => {
  if (isHtml(filename)) return { isValid: true, extension: "html" };
  if (isCSS(filename)) return { isValid: true, extension: "css" };
  if (isJS(filename)) return { isValid: true, extension: "js" };
  return { isValid: false, extension: _getExtension(filename) };
};

/**
 * Represents the state of an extension, if isValid is true then the extension
 * is valid.
 *
 * @typedef {Object} ValidHydriamelExtension
 * @property {string} isValid - Tells is the extension is valid or not.
 * @property {string} extension - The actual extension.
 */
export interface ValidHydriamelExtension {
  isValid: boolean;
  extension: string;
}
