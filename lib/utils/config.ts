import { FileList, getFiles, parseJSON } from "./files.ts";
import * as error from "./errors.ts";

/**
 * Gets the text content of the configuration file if exists.
 *
 * @return {Promise<string | null>} File text content.
 */
export const getHydriamelFile = async (): Promise<string | null> => {
  try {
    return await Deno.readTextFile("hydriamel.json");
  } catch {
    console.info("🍯 Consider using a hydriamel.json file...");
  }
  return null;
};

/**
 * Builds a configuration object (@link HydriamelConfig).
 *
 * @param {string} hydriamelJsonFile - The route of the file is located. If not
 * found then the CLI arguments will be used, if there are not provided then the
 * default configuration is loaded.
 * @return {HydriamelConfig} A ready configuration to start.
 */
export const getHydriamelObj = (
  hydriamelJsonFile: string | null,
): HydriamelConfig => {
  let hydriamelConfig: HydriamelConfig = {
    componentsRoute: "./components",
    templatesRoute: "./templates",
    componentsName: "<COMPONENT-NAME/>",
    distRoute: "./dist",
  };
  if (hydriamelJsonFile) {
    const config = parseJSON(hydriamelJsonFile, () => Deno.exit());
    if (config) {
      const { components, templates, dist } = config;
      hydriamelConfig = {
        componentsRoute: components.folder ?? hydriamelConfig.componentsRoute,
        templatesRoute: templates.folder ?? hydriamelConfig.templatesRoute,
        componentsName: dist.regExp ?? hydriamelConfig.componentsName,
        distRoute: dist.folder ?? hydriamelConfig.distRoute,
      };
    } else {
      const denoArgs = Deno.args;
      hydriamelConfig = {
        componentsRoute: denoArgs[0] ?? hydriamelConfig.componentsRoute,
        templatesRoute: denoArgs[1] ?? hydriamelConfig.templatesRoute,
        componentsName: denoArgs[2] ?? hydriamelConfig.componentsName,
        distRoute: denoArgs[3] ?? hydriamelConfig.distRoute,
      };
    }
  }
  return hydriamelConfig;
};

/**
 * Get all the possibles filename and paths of
 * {@link HydriamelConfig.componentsRoute} and in
 * {@link HydriamelConfig.templatesRoute}.
 *
 * @param {HydriamelConfig} hydriamelObj - The configuration that wil be used to
 * locate all the files.
 * @return {Promise<Record<string, FileList>>} All the possible file paths
 * , refer to it as "componentsFiles" and "templatesFiles".
 */
export const getWorkFlow = async (
  hydriamelObj: HydriamelConfig,
): Promise<Record<string, FileList>> => {
  let componentsFiles: FileList = [];
  let templatesFiles: FileList = [];
  const { componentsRoute, templatesRoute } = hydriamelObj;
  for await (const workspace of Deno.readDir("./")) {
    if (workspace.isDirectory && workspace.name === componentsRoute) {
      componentsFiles = await getFiles(componentsRoute);
    } else if (workspace.isDirectory && workspace.name === templatesRoute) {
      templatesFiles = await getFiles(templatesRoute);
    }
    if (componentsFiles.length !== 0 && templatesFiles.length !== 0) break;
  }
  return {
    componentsFiles,
    templatesFiles,
  };
};

/**
 * Creates the enviorment that it will be work in.
 *
 * Creates and removes the distributable folder. You may consider using this
 * if the distributable folder is meant to be recompiled.
 *
 * @param {string} distRoute - The file route that will contain all the
 * distributable.
 * @return {Promise<void>}
 */
export const createEnviorment = async (distRoute: string): Promise<void> => {
  try {
    await Deno.remove(distRoute, { recursive: true });
  } catch {
    console.info(`No ${distRoute} directory found. Creating one instead...`);
  }
  try {
    await Deno.mkdir(distRoute);
  } catch {
    console.error(error.couldNotMkDir(distRoute));
    Deno.exit();
  }
};

/**
 * The configuration object.
 *
 * @typedef {Object} HydriamelConfig
 * @property {stirng} componentsRoute - The folder that contains all the components.
 * @property {string} templatesRoute - The folder that contains all the templates.
 * @property {string | Record<string, string>} componentsName - The component name placeholder.
 * @property {number} distRoute - The folder that will have all the distributables.
 */
export interface HydriamelConfig {
  componentsRoute: string;
  templatesRoute: string;
  componentsName: string | Record<string, string>;
  distRoute: string;
}
