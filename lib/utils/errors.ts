/**
 * Returns what happend while trying to find the componentsFolder and the
 * templatesFolder.
 *
 * @param {string} componentsFolder - The folder name were all the components
 * are located.
 * @param {string} templatesFolder - The folder name were all the templates are
 * located.
 * @return {string} The error description.
 */
export const noConfigFolders = (
  componentsFolder: string,
  templatesFolder: string,
): string =>
  `Check if the ${componentsFolder} and ${templatesFolder} folder are in the
currentdirectory.`;

/**
 * Returns what happend while trying to create the directory "distRoute".
 *
 * @param {string} directoryRoute - The route of the directory.
 * @return {string} The error description.
 */
export const couldNotMkDir = (directoryRoute: string): string =>
  `${directoryRoute} directory could not be created.`;
