import { validHydriamelExtension } from "./extensions.ts";

/**
 * Gets a {@link FileList} in the workspace.
 *
 * @example
 * getFiles("./my/workspace", undefined); // [{path: "./path/in/workspace", ...}]
 *
 * @param {string} workspace - The workspace that the files will be looked up.
 * @param {FileList} fileList - You never  will fill this parameter unless you
 * got a {@link FileList} that it will get append it too.
 * @return {Promise<FileList>} A list of all the files and dirs in the
 * workspace.
 */
export const getFiles = async (
  workspace: string,
  fileList?: FileList,
): Promise<FileList> => {
  const objFiles: FileList = fileList ? fileList : [];
  for await (const fileOrDir of Deno.readDir(workspace)) {
    const validExt = validHydriamelExtension(fileOrDir.name);
    if (fileOrDir.isFile && validExt.isValid) {
      objFiles.push({
        path: workspace,
        file: fileOrDir.name,
        extension: validExt.extension,
      });
    }
    if (fileOrDir.isDirectory) {
      objFiles.push({ path: workspace, dir: fileOrDir.name });
      return getFiles(`${workspace}/${fileOrDir.name}`, objFiles);
    }
  }
  return objFiles;
};

/**
 * Gets a {@link JSON} object with the provided {@param file}.
 *
 * @param {string} file - The .json string.
 * @param {() => void} onFail - If the parse fails then this is executed.
 * @return {void | Record<string,  any>}
 */
export const parseJSON = (
  jsonStr: string,
  onFail: () => void,
): void | Record<string, any> => {
  try {
    return JSON.parse(jsonStr);
  } catch {
    console.error(`${jsonStr} is not formatted correctly.`);
    onFail();
  }
};

/**
 * Returns if the parameter {@param probablyDir} is a folder. If the dir is
 * is not found or is a file then `false` is returned, otherwised is `true`.
 *
 * @param {string} probablyDir - The name of the dir, be sure to provide the
 * correct path.
 * @param {() => void} onFail - If the parse fails then this is executed.
 * @return {Promise<boolean>}
 */
export const isFolder = async (probablyDir: string): Promise<boolean> => {
  try {
    const fileInfo = await Deno.lstat(probablyDir);
    return fileInfo.isDirectory;
  } catch {
    return false;
  }
};

/**
 * Removes the last char of a Uint8Array if it's a new line.
 *
 * @param {Uint8Array} data - An Uint8Array object.
 */
export const removeLastNewLine = (data: Uint8Array): Uint8Array => {
  const isNewLine = data[data.length - 1] == 10;
  if (isNewLine) {
    return data.slice(0, -1);
  }
  return data;
};

/**
 * Contains the information of a file, e.g. the name and the path that belongs
 * to it.
 * @example
 * const file: File = {path: "./my/path", file: "filename.txt"};
 * @typedef {Object} File
 * @property {string} path - The path to this file.
 * @property {string | null} file - The filename of the file, if it's a file.
 * @property {number | null} dir - The dirname of the dir, if it's a dir.
 */
export interface File {
  path: string;
  file?: string;
  extension?: string;
  dir?: string;
}

/**
 * An list of {@link File}s.
 * @augments Array
 *
 * @typedef {Array} FileList
 * @property {File} file - The current {@link File}.
 */
export interface FileList extends Array<File> {
  [index: number]: File;
}
