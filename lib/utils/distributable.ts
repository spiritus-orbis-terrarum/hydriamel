import { File, removeLastNewLine } from "./files.ts";
import { HydriamelConfig } from "./config.ts";

/**
 * Makes a directory in the provided file.path named file.dir.
 *
 * @param {File} file - The file to be created, must be directory.
 * @param {HydriamelConfig} hydriamelObj - The configuration object.
 */
export const builDir = async (hydriamelObj: HydriamelConfig, file: File) => {
  const { path, dir } = file;
  const { distRoute } = hydriamelObj;
  const removedComponentsPath = path.split("/").slice(1, path.length).join("/");
  const foldersParsed = removedComponentsPath !== ""
    ? removedComponentsPath + "/"
    : removedComponentsPath;
  const fullPath = `${foldersParsed}${file.dir}`;
  const dirRoute = `${distRoute}/${fullPath}`;
  try {
    await Deno.mkdir(dirRoute);
    console.info(`${dir} created in ${fullPath}.`);
  } catch {
    console.info(`${dir} already created in ${fullPath}.`);
  }
};

/**
 * Injects the component file content into a template file content.
 *
 * @param {HydriamelConfig} hydriamelObj - The configuration object.
 * @param {File} templateFile
 * @param {File} componentFile
 */
export const buildFile = async (
  hydriamelObj: HydriamelConfig,
  templateFile: File,
  componentFile: File,
) => {
  if (templateFile?.extension !== componentFile?.extension) return;
  let tempPath = templateFile.path;
  const decoder = new TextDecoder("utf-8");
  const {
    componentsRoute,
    templatesRoute,
    componentsName,
    distRoute,
  } = hydriamelObj;
  const templatePath = `${templateFile.path}/${templateFile.file}`;
  const templateData = await Deno.readFile(templatePath);
  const templateContent = decoder.decode(removeLastNewLine(templateData));
  const componentPath = `${componentFile.path}/${componentFile.file}`;
  const componentData = await Deno.readFile(componentPath);
  const componentContent = decoder.decode(removeLastNewLine(componentData));
  const folders = tempPath.split("/");
  if (folders[0] === componentsRoute || folders[0] === templatesRoute) {
    tempPath = folders.slice(1, folders.length).join("/");
  }
  const file = `${distRoute}/${
    tempPath !== "" ? tempPath + "/" : tempPath
  }${templateFile.file}`;
  if (componentFile.file) {
    const componentF = componentFile.file;
    const nameWithoutExtension = componentF.split(".").slice(0, -1).join(".");
    let component;
    if (typeof componentsName === "string") {
      component = componentsName.replace(
        "COMPONENT-NAME",
        nameWithoutExtension === undefined ? "" : nameWithoutExtension,
      );
    }
    if (typeof componentsName === "object") {
      let extension;
      if (templateFile.extension) {
        extension = componentsName[templateFile.extension];
      }
      if (extension) {
        component = extension.replace(
          "COMPONENT-NAME",
          nameWithoutExtension === undefined ? "" : nameWithoutExtension,
        );
      }
    }
    let newTemplateContent;
    try {
      const newTemplateData = await Deno.readFile(file);
      newTemplateContent = decoder.decode(removeLastNewLine(newTemplateData));
    } catch {
      await Deno.create(file);
      console.info(`${file} was created...`);
    }
    const template2BeUsed = newTemplateContent === undefined
      ? templateContent
      : newTemplateContent;
    if (template2BeUsed && component) {
      let templateFormatted = template2BeUsed.replace(
        component,
        templateFile.extension === "js"
          ? `{${componentContent}}`
          : componentContent,
      );
      await Deno.writeTextFile(file, templateFormatted);
      console.info(`${component} was injected in ${file}`);
    } else {
      console.error("Unexpected error...");
    }
  }
};
