# 🍯 Hydriamel

Hydriamel is not a framework or a library is a tool that intends to
simplify, help and improve the web development experience for the developer
without sacrifying the things that you already know.

## Get started

* You can [get the artifact][1] that is compatible with your OS or if you have
Deno installed follow [this][2] tutorial. Install it to get started with
Hydriamel.

Follow this [tutorial][3] to learn how Hydriamel works.

## Problem that is aiming to solve

- Have reusable parts of your code that you can use as many times you want
  (`components`) without learning a new way to do web development.

- Have less bloated files, usually HTML/CSS files have a lot of lines of code and
  the solutions to this is to use a framework.

- Have a scalable way to do web development without altering what you already
  know of it.

- Do one task only and make the developer think about the rest.

## Why you may like this CLI.

First be **aware** that this CLI doesn't fulfill with the [guidelines][4] that
needs to have a CLI, is more an executable than anything else. This is for
developers that wants to oragnize their projects without learning something new
in the web development area that does the same thing they already know. These
days a new JS framework comes trendy every month and developers overthink if
they need to learn it just to land a job or just because they think that's the
way to do it, but maybe you [may not need a framework][5] and that's fine.

_This CLI is in ALPHA, new features will be added in the future or some
functionalities may break_

[1]: https://gitlab.com/spiritus-orbis-terrarum/hydriamel/-/jobs/1201559441/artifacts/browse
[2]: https://gitlab.com/spiritus-orbis-terrarum/hydriamel/-/wikis/Create%20an%20executable
[3]: https://gitlab.com/spiritus-orbis-terrarum/hydriamel/-/wikis/Getting-started
[4]: https://clig.dev/
[5]:https://www.youtube.com/watch?v=k7n2xnOiWI8
