export const configuration = `
{
  "dist": {
    "folder": "dist",
    "regExp": {
      "html": "<COMPONENT-NAME/>",
      "css": "<!--COMPONENT-NAME-->",
      "js": "/*COMPONENT-NAME*/"
    }
  },
  "templates": {
    "folder": "templates"
  },
  "components": {
    "folder": "components"
  }
}
`;
