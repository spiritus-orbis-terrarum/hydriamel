import { cssTest } from "./basicFunctionality/css.ts";
import { htmlTest } from "./basicFunctionality/html.ts";
import { javascriptTest } from "./basicFunctionality/javascript.ts";

Deno.test({
  name: "-> Making sure CSS is compatible with Hydriamel",
  fn: cssTest,
});
Deno.test({
  name: "-> Making sure HTML is compatible with Hydriamel",
  fn: htmlTest,
});
Deno.test({
  name: "-> Making sure JS is compatible with Hydriamel",
  fn: javascriptTest,
});
