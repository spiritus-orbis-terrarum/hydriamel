import { assertEquals } from "https://deno.land/std@0.92.0/testing/asserts.ts";
import * as g from "../globals.ts";

export const singleExtensionTest = async (
  contentObj: Record<string, string>,
  templateComponentObj: Record<string, string>,
  ext: string,
  content?: string
) => {
  // Create the enviorment.
  const workingDir = Deno.makeTempDirSync({ prefix: "hydriamel" });
  Deno.mkdirSync(`${workingDir}/components`);
  Deno.mkdirSync(`${workingDir}/templates`);

  // Create the configurations of the enviorment.
  Deno.writeTextFileSync(`${workingDir}/hydriamel.json`, g.configuration);

  // Create the components and add the content to it.
  const headerContent = contentObj.header;
  const bodyContent = contentObj.body;
  const footerContent = contentObj.footer;
  if (!content) {
    content = headerContent + "\n" + bodyContent + "\n" + footerContent;
  }
  const componentsRoute = `${workingDir}/components`;
  Deno.writeTextFileSync(`${componentsRoute}/Header.${ext}`, headerContent);
  Deno.writeTextFileSync(`${componentsRoute}/Body.${ext}`, bodyContent);
  Deno.writeTextFileSync(`${componentsRoute}/Footer.${ext}`, footerContent);

  // Create the template and add the content to it.
  const headerTemplateContent = templateComponentObj.header;
  const bodyTemplateContent = templateComponentObj.body;
  const footerTemplateContent = templateComponentObj.footer;
  const templateTempContent = [];
  templateTempContent.push(headerTemplateContent);
  templateTempContent.push(bodyTemplateContent);
  templateTempContent.push(footerTemplateContent);
  const tTempCont = templateTempContent.join("\n");
  const tTempContRoute = `${workingDir}/templates/Template.${ext}`;
  Deno.writeTextFileSync(tTempContRoute, tTempCont);

  // Take the entry point of the hydriamel.
  const entrypointRoute = Deno.realPathSync("lib/app.ts");

  // Change the current directory to the workingDir to run hydriamel in the
  // entrypointRoute. This will change the tTempCont.
  Deno.chdir(workingDir);
  const p = Deno.run({
    cmd: ["deno", "run", "--allow-read", "--allow-write", entrypointRoute],
  });

  // Await for the process to complete and store the exit code.
  // These resources should be closed after you are done using them.
  // Read more here: https://deno.land/manual@v1.8.3/contributing/architecture
  await p.status();
  p.close();

  // Read the template content.
  const path2Template = tTempContRoute.split("/");
  const templateFileName = path2Template[path2Template.length - 1];
  const templateContent = Deno.readTextFileSync(`dist/${templateFileName}`);

  // Clean the enviorment because it's not needed anymore. And change back to
  // the entrypointRoute folder.
  Deno.removeSync(workingDir, { recursive: true });
  const entrypointRouteFolders = entrypointRoute.split("/");
  entrypointRouteFolders.pop();
  entrypointRouteFolders.pop();
  Deno.chdir(entrypointRouteFolders.join("/"));

  // Make sure this two strings are equal.
  assertEquals(content, templateContent);
};
