import { singleExtensionTest } from "./_singleExtensionTest.ts";

const scopeJS = (content: string) => `{${content}}`;

export const javascriptTest = async () => {
  const templateComponentObj = {
    header: "/*Header*/",
    body: "/*Body*/",
    footer: "/*Footer*/",
  };
  const header = "const header = 'A header'";
  const body = "const body = 'A body'";
  const footer = "const footer = 'A footer'";
  const contentObj = { header, body, footer };
  const extension = "js";
  const content = scopeJS(header) + "\n" + scopeJS(body) + "\n" +
    scopeJS(footer);
  console.log(content);
  return await singleExtensionTest(
    contentObj,
    templateComponentObj,
    extension,
    content,
  );
};
