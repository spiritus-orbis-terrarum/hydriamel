import { singleExtensionTest } from "./_singleExtensionTest.ts";

export const htmlTest = async () => {
  const templateComponentObj = {
    header: "<Header/>",
    body: "<Body/>",
    footer: "<Footer/>",
  };
  const contentObj = {
    header: "<header>Header</header>",
    body: "<main>Body</main>",
    footer: "<footer>Footer</footer>",
  };
  const extension = "html";
  return await singleExtensionTest(contentObj, templateComponentObj, extension);
};
