import { singleExtensionTest } from "./_singleExtensionTest.ts";

export const cssTest = async () => {
  const templateComponentObj = {
    header: "<!--Header-->",
    body: "<!--Body-->",
    footer: "<!--Footer-->",
  };
  const contentObj = {
    header: "header {color: red}",
    body: "main {color: blue}",
    footer: "footer {color: yellow}",
  };
  const extension = "css";
  return await singleExtensionTest(contentObj, templateComponentObj, extension);
};
